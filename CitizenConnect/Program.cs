using CitizenConnect.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using ServiceStack.Text;
using System.IO;

var builder = WebApplication.CreateBuilder(args);
const string StrategieCors = "StrategieGlobale";

// Add cores
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: StrategieCors,
        policy =>
        {
            policy.WithOrigins("http://localhost:3011").AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin();
        });
});


// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddDbContext<TodoContext>(opt => opt.UseNpgsql(builder.Configuration.GetConnectionString("con")));

builder.Services.AddDbContext<TodoContext>(opt => opt.UseInMemoryDatabase("TodoList"));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    
}

// Add StaticFiles("Image")
app.UseStaticFiles();

app.UseStaticFiles(new StaticFileOptions
{
    FileProvider = new PhysicalFileProvider(
        Path.Combine(Directory.GetCurrentDirectory(), "Upload/Files")),
    RequestPath = "/Upload/Files"
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseCors(StrategieCors);

app.MapControllers();

app.Run();



