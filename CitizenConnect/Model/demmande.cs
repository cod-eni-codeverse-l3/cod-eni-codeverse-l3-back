﻿using System.ComponentModel.DataAnnotations;

namespace CitizenConnect.Model
{
    public class demmande
    {
        [Key]
        public string idDemmande { get; set; }
        public string serviceDemmande { get; set; }
        public DateOnly dateDemmande  { get; set; }
        public string numCINDemmande { get; set; }
        public string idTicket { get; set; }
        public string numTel { get; set; }
        public string RefMvola { get; set; }
        public string listeFic { get; set; }
        public string infoRequis { get; set; }
    }
}
