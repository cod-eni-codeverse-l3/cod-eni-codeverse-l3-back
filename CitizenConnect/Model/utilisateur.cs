﻿using System.ComponentModel.DataAnnotations;

namespace CitizenConnect.Model
{
    public class utilisateur
    {
        [Key]
        public string numCIN { get; set; }
        public string role { get; set; }
        public string adresse { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string sexe { get; set; }
        public DateOnly datenaiss  { get; set; }
        public string proffession { get; set; }
        public string photo { get; set; }
        public string CINrecto { get; set; }
        public string CINverso { get; set; }
        public string etat { get; set; }
        public string mdp { get; set; }
    }
}
