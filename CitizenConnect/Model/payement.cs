﻿using System.ComponentModel.DataAnnotations;

namespace CitizenConnect.Model
{
    public class payement
    {

        [Key]
        public string idPayemment { get; set; }
        public DateOnly datePayement { get; set; }
        public string numCIN { get; set; }
        public string idRappel { get; set; }
        public string numTel { get; set; }
        public string RefMvola { get; set; }
    }
}
