﻿using Microsoft.EntityFrameworkCore;

namespace CitizenConnect.Model;

public class TodoContext : DbContext
{
    public TodoContext(DbContextOptions<TodoContext> options)
        : base(options)
    {
    }

    public DbSet<demmande> demmandes { get; set; } = null!;
    public DbSet<payement> payements { get; set; } = null!;
    public DbSet<rappel> rappels { get; set; } = null!;
    public DbSet<ticket> tickets { get; set; } = null!;
    public DbSet<utilisateur> utilisateurs { get; set; } = null!;
   


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=Citizen;Username=postgres;Password=010203");
    }
}