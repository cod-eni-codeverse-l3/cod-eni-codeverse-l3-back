﻿using System.ComponentModel.DataAnnotations;

namespace CitizenConnect.Model
{
    public class rappel
    {
        [Key]
        public string idRappel { get; set; }
        public string sommeR { get; set; }
        public string Description { get; set; }
        public string numCINCitoyen { get; set; }
        public string etatRappel { get; set; }
    }
}
