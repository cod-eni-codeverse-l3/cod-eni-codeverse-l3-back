﻿using System.ComponentModel.DataAnnotations;

namespace CitizenConnect.Model
{
    public class ticket
    {
        [Key]
        public string idTicket { get; set; }
        public string numCIN { get; set; }
        public string sommeT { get; set; }
        public string idDemmande { get; set; }
        public string LienRecep { get; set; }
        public DateOnly dateRecep { get; set; }
    }
}
