﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CitizenConnect.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "demmandes",
                columns: table => new
                {
                    idDemmande = table.Column<string>(type: "text", nullable: false),
                    serviceDemmande = table.Column<string>(type: "text", nullable: false),
                    dateDemmande = table.Column<DateOnly>(type: "date", nullable: false),
                    numCINDemmande = table.Column<string>(type: "text", nullable: false),
                    idTicket = table.Column<string>(type: "text", nullable: false),
                    numTel = table.Column<string>(type: "text", nullable: false),
                    RefMvola = table.Column<string>(type: "text", nullable: false),
                    listeFic = table.Column<string>(type: "text", nullable: false),
                    infoRequis = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_demmandes", x => x.idDemmande);
                });

            migrationBuilder.CreateTable(
                name: "payements",
                columns: table => new
                {
                    idPayemment = table.Column<string>(type: "text", nullable: false),
                    datePayement = table.Column<DateOnly>(type: "date", nullable: false),
                    numCIN = table.Column<string>(type: "text", nullable: false),
                    idRappel = table.Column<string>(type: "text", nullable: false),
                    numTel = table.Column<string>(type: "text", nullable: false),
                    RefMvola = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_payements", x => x.idPayemment);
                });

            migrationBuilder.CreateTable(
                name: "rappels",
                columns: table => new
                {
                    idRappel = table.Column<string>(type: "text", nullable: false),
                    sommeR = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    numCINCitoyen = table.Column<string>(type: "text", nullable: false),
                    etatRappel = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rappels", x => x.idRappel);
                });

            migrationBuilder.CreateTable(
                name: "tickets",
                columns: table => new
                {
                    idTicket = table.Column<string>(type: "text", nullable: false),
                    numCIN = table.Column<string>(type: "text", nullable: false),
                    sommeT = table.Column<string>(type: "text", nullable: false),
                    idDemmande = table.Column<string>(type: "text", nullable: false),
                    LienRecep = table.Column<string>(type: "text", nullable: false),
                    dateRecep = table.Column<DateOnly>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tickets", x => x.idTicket);
                });

            migrationBuilder.CreateTable(
                name: "utilisateurs",
                columns: table => new
                {
                    numCIN = table.Column<string>(type: "text", nullable: false),
                    role = table.Column<string>(type: "text", nullable: false),
                    adresse = table.Column<string>(type: "text", nullable: false),
                    nom = table.Column<string>(type: "text", nullable: false),
                    prenom = table.Column<string>(type: "text", nullable: false),
                    sexe = table.Column<string>(type: "text", nullable: false),
                    datenaiss = table.Column<DateOnly>(type: "date", nullable: false),
                    proffession = table.Column<string>(type: "text", nullable: false),
                    photo = table.Column<string>(type: "text", nullable: false),
                    CINrecto = table.Column<string>(type: "text", nullable: false),
                    CINverso = table.Column<string>(type: "text", nullable: false),
                    etat = table.Column<string>(type: "text", nullable: false),
                    mdp = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_utilisateurs", x => x.numCIN);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "demmandes");

            migrationBuilder.DropTable(
                name: "payements");

            migrationBuilder.DropTable(
                name: "rappels");

            migrationBuilder.DropTable(
                name: "tickets");

            migrationBuilder.DropTable(
                name: "utilisateurs");
        }
    }
}
