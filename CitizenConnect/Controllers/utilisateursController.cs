﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CitizenConnect.Model;

namespace CitizenConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class utilisateursController : ControllerBase
    {
        private readonly TodoContext _context;

        public utilisateursController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/utilisateurs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<utilisateur>>> Getutilisateurs()
        {
            return await _context.utilisateurs.ToListAsync();
        }

        // GET: api/utilisateurs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<utilisateur>> Getutilisateur(string id)
        {
            var utilisateur = await _context.utilisateurs.FindAsync(id);

            if (utilisateur == null)
            {
                return NotFound();
            }

            return utilisateur;
        }

        // PUT: api/utilisateurs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> Pututilisateur(string id, utilisateur utilisateur)
        {
            if (id != utilisateur.numCIN)
            {
                return BadRequest();
            }

            _context.Entry(utilisateur).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!utilisateurExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/utilisateurs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<utilisateur>> Postutilisateur(utilisateur utilisateur)
        {
            _context.utilisateurs.Add(utilisateur);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (utilisateurExists(utilisateur.numCIN))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("Getutilisateur", new { id = utilisateur.numCIN }, utilisateur);
        }

        // DELETE: api/utilisateurs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deleteutilisateur(string id)
        {
            var utilisateur = await _context.utilisateurs.FindAsync(id);
            if (utilisateur == null)
            {
                return NotFound();
            }

            _context.utilisateurs.Remove(utilisateur);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool utilisateurExists(string id)
        {
            return _context.utilisateurs.Any(e => e.numCIN == id);
        }
    }
}
