﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CitizenConnect.Model;

namespace CitizenConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class demmandesController : ControllerBase
    {
        private readonly TodoContext _context;

        public demmandesController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/demmandes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<demmande>>> Getdemmandes()
        {
            return await _context.demmandes.ToListAsync();
        }

        // GET: api/demmandes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<demmande>> Getdemmande(string id)
        {
            var demmande = await _context.demmandes.FindAsync(id);

            if (demmande == null)
            {
                return NotFound();
            }

            return demmande;
        }

        // PUT: api/demmandes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> Putdemmande(string id, demmande demmande)
        {
            if (id != demmande.idDemmande)
            {
                return BadRequest();
            }

            _context.Entry(demmande).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!demmandeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/demmandes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<demmande>> Postdemmande(demmande demmande)
        {
            _context.demmandes.Add(demmande);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (demmandeExists(demmande.idDemmande))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("Getdemmande", new { id = demmande.idDemmande }, demmande);
        }

        // DELETE: api/demmandes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletedemmande(string id)
        {
            var demmande = await _context.demmandes.FindAsync(id);
            if (demmande == null)
            {
                return NotFound();
            }

            _context.demmandes.Remove(demmande);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool demmandeExists(string id)
        {
            return _context.demmandes.Any(e => e.idDemmande == id);
        }
    }
}
