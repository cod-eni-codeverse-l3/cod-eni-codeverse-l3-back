﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CitizenConnect.Model;

namespace CitizenConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class payementsController : ControllerBase
    {
        private readonly TodoContext _context;

        public payementsController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/payements
        [HttpGet]
        public async Task<ActionResult<IEnumerable<payement>>> Getpayements()
        {
            return await _context.payements.ToListAsync();
        }

        // GET: api/payements/5
        [HttpGet("{id}")]
        public async Task<ActionResult<payement>> Getpayement(string id)
        {
            var payement = await _context.payements.FindAsync(id);

            if (payement == null)
            {
                return NotFound();
            }

            return payement;
        }

        // PUT: api/payements/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> Putpayement(string id, payement payement)
        {
            if (id != payement.idPayemment)
            {
                return BadRequest();
            }

            _context.Entry(payement).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!payementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/payements
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<payement>> Postpayement(payement payement)
        {
            _context.payements.Add(payement);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (payementExists(payement.idPayemment))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("Getpayement", new { id = payement.idPayemment }, payement);
        }

        // DELETE: api/payements/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletepayement(string id)
        {
            var payement = await _context.payements.FindAsync(id);
            if (payement == null)
            {
                return NotFound();
            }

            _context.payements.Remove(payement);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool payementExists(string id)
        {
            return _context.payements.Any(e => e.idPayemment == id);
        }
    }
}
