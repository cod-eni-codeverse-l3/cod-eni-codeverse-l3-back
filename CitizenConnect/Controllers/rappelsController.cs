﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CitizenConnect.Model;

namespace CitizenConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class rappelsController : ControllerBase
    {
        private readonly TodoContext _context;

        public rappelsController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/rappels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<rappel>>> Getrappels()
        {
            return await _context.rappels.ToListAsync();
        }

        // GET: api/rappels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<rappel>> Getrappel(string id)
        {
            var rappel = await _context.rappels.FindAsync(id);

            if (rappel == null)
            {
                return NotFound();
            }

            return rappel;
        }

        // PUT: api/rappels/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> Putrappel(string id, rappel rappel)
        {
            if (id != rappel.idRappel)
            {
                return BadRequest();
            }

            _context.Entry(rappel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!rappelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/rappels
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<rappel>> Postrappel(rappel rappel)
        {
            _context.rappels.Add(rappel);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (rappelExists(rappel.idRappel))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("Getrappel", new { id = rappel.idRappel }, rappel);
        }

        // DELETE: api/rappels/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deleterappel(string id)
        {
            var rappel = await _context.rappels.FindAsync(id);
            if (rappel == null)
            {
                return NotFound();
            }

            _context.rappels.Remove(rappel);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool rappelExists(string id)
        {
            return _context.rappels.Any(e => e.idRappel == id);
        }
    }
}
